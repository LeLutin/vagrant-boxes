#!/bin/sh -eux
#
# This code is available under the GPLv3 license. A copy of the license should
# be available in the same code repository
#

apt-get update;
apt-get -y upgrade;

if [ -d /etc/init ]; then
    # update package index on boot
    cat <<EOF >/etc/init/refresh-apt.conf;
description "update package index"
start on networking
task
exec /usr/bin/apt-get update
EOF
fi
