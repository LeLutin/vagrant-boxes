#!/bin/sh -eux
#
# This code is available under the GPLv3 license. A copy of the license should
# be available in the same code repository
#

# Bring back eth0. The "predictable" interface names from systemd actually
# change between box building and vagrant vm.
sed -i 's/^\(GRUB_CMDLINE_LINUX=".*\)"$/\1 net.ifnames=0 biosdevname=0"/' /etc/default/grub
update-grub2

# Make sure that eth0 is known
sed -i 's/^\(allow-hotplug\) en.*$/\1 eth0/;s/^\(iface\) en\w\+ \(inet dhcp\)$/\1 eth0 \2/' /etc/network/interfaces

