#!/bin/sh -eux
#
# This code is available under the GPLv3 license. A copy of the license should
# be available in the same code repository
#

# bring grub timeout down to 1s to speed up boot time
sed -i 's/^\(GRUB_TIMEOUT\)=.*$/\1=1/' /etc/default/grub
update-grub2

