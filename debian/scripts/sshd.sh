#!/bin/sh -eux
#
# This code is available under the GPLv3 license. A copy of the license should
# be available in the same code repository
#

echo "UseDNS no" >>/etc/ssh/sshd_config;
echo "GSSAPIAuthentication no" >>/etc/ssh/sshd_config;
