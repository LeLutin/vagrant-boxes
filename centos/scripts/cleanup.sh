#
# This file is under the Mozilla Public License Version 2.0 license
# A copy of the license text should have been delivered along with this file
#

#remove network mac and interface information
sed -i '/HWADDR/d' /etc/sysconfig/network-scripts/ifcfg-eth0
sed -i "/^UUID/d" /etc/sysconfig/network-scripts/ifcfg-eth0

#disable selinux
rm /etc/sysconfig/selinux
ln -s /etc/selinux/config /etc/sysconfig/selinux
sed -i "s/^\(SELINUX=\).*/\1disabled/g" /etc/selinux/config

#remove any ssh keys (if requested) or persistent routes, dhcp leases
if [ "${GENERATE_NEW_SSH_KEYS}" != "false" ]; then
  rm -f /etc/ssh/ssh_host_*
fi
rm -f /etc/udev/rules.d/70-persistent-net.rules
rm -f /var/lib/dhclient/dhclient-eth0.leases
rm -rf /tmp/*
yum -y clean all
