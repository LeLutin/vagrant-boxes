#!/bin/sh
#
# This file is under the Mozilla Public License Version 2.0 license
# A copy of the license text should have been delivered along with this file
#

# Store build time
date > /etc/vagrant_box_build_time

# Install vagrant key
mkdir -pm 700 /home/vagrant/.ssh
curl -L https://raw.github.com/mitchellh/vagrant/master/keys/vagrant.pub -o /home/vagrant/.ssh/authorized_keys
chmod 0600 /home/vagrant/.ssh/authorized_keys
chown -R vagrant:vagrant /home/vagrant/.ssh

