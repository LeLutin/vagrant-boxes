#!/bin/sh -eux

if [ -f /boot/grub/grub.conf ]; then
  # centos6 uses grub1 and we need to change the configuration manually
  sed -i -e 's/^\(timeout\)=.*$/\1=1/' /boot/grub/grub.conf
else
  sed -i "s/^\(GRUB_TIMEOUT\)=[0-9]\+/\1=1/" /etc/default/grub
  grub2-mkconfig --output=/boot/grub2/grub.cfg
fi

