#!/bin/sh -eux

# see https://www.how2centos.com/centos-6-puppet-install/

# Puppet from EL6 and EL7 is too old, we need to install it from upstream.
# We'll use Puppet 5 Platform
release=$(cat /etc/redhat-release | sed 's/^.*release //;s/\..*$//')
case $release in
  "6") rpm -Uvh https://yum.puppet.com/puppet5/puppet5-release-el-6.noarch.rpm
    ;;
  "7") rpm -Uvh https://yum.puppet.com/puppet5/puppet5-release-el-7.noarch.rpm
    ;;
  *) echo "CentOS release $release is not currently supported" >&2
    exit 1
    ;;
esac

yum install -y puppet-agent
ln -s /opt/puppetlabs/bin/puppet /usr/bin/puppet
ln -s /opt/puppetlabs/bin/facter /usr/bin/facter
ln -s /opt/puppetlabs/bin/hiera /usr/bin/hiera
ln -s /opt/puppetlabs/bin/mco /usr/bin/mco

