#
# This file is under the Mozilla Public License Version 2.0 license
# A copy of the license text should have been delivered along with this file
#
yum -y update
yum -y upgrade

if [ -d /etc/init ]; then
    # update package index on boot
    cat <<EOF >/etc/init/refresh-yum.conf;
description "update package index"
start on networking
task
exec /usr/bin/yum -y update
EOF
fi

