#!/bin/sh -eux

echo "UseDNS no" >>/etc/ssh/sshd_config;
sed -i "s/^\(GSSAPIAuthentication\) yes$/\1 no/" /etc/ssh/sshd_config;

