# vagrant boxes

Templates for creating vagrant boxes for use with vagrant-libvirt, and that aim
to make it easy to provision with puppet and test puppet manifests in them.

Thanks to skamithi for creating the initial readme. Check out his blog
for creating [libvirt-vagrant](http://linuxsimba.com/building-qcow-vagrant-box/)
compatible boxes.

Forked from [packer-libvirt-profiles][https://github.com/jtoppins/packer-libvirt-profiles] by jtoppins.

## List of templates

 * debian 9 (stretch)
 * debian 8 (jessie)
 * centos 7
 * centos 6

## License

The template for centos 6 was initially copied from
[packer-qemu-templates][https://github.com/jakobadam/packer-qemu-templates] but
was modified to look like other templates here. It was also changed to use a
more recent release of centos 6. Because of its origin, the files of this
template are under the Mozilla Public License 2.0 license.

All other files are under the GPLv3.0+ license.

## Requirements
* Install [Libvirt](http://libvirt.org)

### Download Packer

[Packer](http://www.packer.io/intro) is for creating machine images from a
single source configuration.

```
apt install packer
```

## Build a Libvirt Compatible Box.

```
cd vagrant-boxes
packer build debian/debian-9-amd64.json
```

You need to be in the same directory as the template to build it, otherwise it
won't find the preseed/kickstart file and build output directory.

By default all builds are headless so you won't see what is happening. If you
want visual cues about what's happening you can connect to the VNC port that
will be shown at build start. If you want to run with the display, so without
headless mode, make sure to set the template variable for that. Headless mode
usually has less problems.

All resulting boxes will be created in the "builds" directory at the top level
of the repository.

### Import the box

```
vagrant box add builds/debian-9-amd64.libvirt.box --name debian-9-amd64
```

### Template variables

You can pass in some variables to the templates either with packer's `-var` or
`-var-file` arguments. To see all variables and their default values you can
use `packer inspect debian/debian-9-amd64.json`.

 * version : version tag for the box. this is just used in the box file name to
   have an indication of what versions were actually built however it cannot be
   used to indicate a version for the box if you add it locally since only
   boxes that are downloaded from the cloud can be versioned (this is a stupid
   restriction imho). normally the version tag should be bumped and maintained
   by this project but if you want to for some reason you can change the
   version tag at build time with this variable.
 * template : main part of the box name
 * headless : set to false to show a display. with headless mode you can still
   connect with your provider's normal method with VNC (defaults to true)
 * ssh_wait_timeout : how much time should packer wait for ssh to become
   available. This can be set higher for systems that take more time to process
   OS installation.
 * http_proxy : proxy server for http on port 80. this is used by wget/curl in
   provisioning scripts (defaults to taking in values from environment to
   re-use user's proxy settings)
 * https_proxy : proxy server for https on port 443. this is used by wget/curl
   in provisioning scripts (defaults to taking in values from environment to
   re-use user's proxy settings)
 * no_proxy : comma-separated list of domains that shouldn't be proxied. this
   is used by wget/curl in provisioning scripts (defaults to taking in values
   from environment to re-use user's proxy settings)
 * iso_checksum : checksum that's used for verifying validity of iso file.
   should be changed if you pass in a different iso name
 * iso_checksum_type : hash algorithm used to compute iso_checksum
 * iso_name : name of the iso file requested from the mirror
 * mirror : mirror URL
 * mirror_directory : directory between the mirror's top and the iso image
   file. This is usually set to the part of the URL that's variable depending
   on the release number that's downloaded.
 * preseed_path : file name in "http" directory that is used for preseeding
   debian's installer with configuration values.
 * kickstart_path : file name in "http" directory that is used to configure
   centos automatic install.
 * user : ssh user that should be created
 * password": password that should be set to the ssh user

